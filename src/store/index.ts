import Steeping from '@/interfaces/Steeping';
import Tea from '@/interfaces/Tea';
import api from '@/lib/Api';
import { createLogger, createStore } from 'vuex';

interface SteepingsSet {
  [index: number]: Steeping[]
}

export default createStore({
  strict: true,

  plugins: [createLogger()],

  state: {
    teas: [] as Tea[],
    steepings: {} as SteepingsSet,
  },

  getters: {
    getTeas: (state) => () => state.teas,
    getSteepingsForTea: (state) => (teaId: number) => state.steepings[teaId],
  },

  mutations: {
    addTea(state, tea: Tea | Tea[]) {
      let teas: Tea[];

      if (Array.isArray(tea)) {
        teas = tea;
      } else {
        teas = [tea];
      }

      const existingIds: Set<number> = new Set(state.teas.map((t) => t.id));
      const newIds: Set<number> = new Set(teas.map((t) => t.id));
      const difference: Set<number> = new Set([...newIds].filter((id) => !existingIds.has(id)));

      const idsToAdd: number[] = Array.from(difference);
      const teasToAdd: Tea[] = teas.filter((t) => idsToAdd.includes(t.id));

      state.teas.push(...teasToAdd);
    },

    removeTea(state, payload: { teaId: number }) {
      const { teaId } = payload;
      const newTeas = state.teas.filter((tea) => tea.id !== teaId);
      state.teas = newTeas;
      delete state.steepings[teaId];
    },

    addSteeping(state, steeping: Steeping) {
      if (Array.isArray(state.steepings[steeping.tea_id])) {
        state.steepings[steeping.tea_id].push(steeping);
      } else {
        state.steepings[steeping.tea_id] = [steeping];
      }
    },

    addSteepings(state, payload: {teaId: number, steepings: Steeping | Steeping[]}) {
      const { teaId, steepings } = payload;
      if (Array.isArray(state.steepings[teaId])) {
        if (Array.isArray(steepings)) {
          state.steepings[teaId].concat(steepings);
        } else {
          state.steepings[teaId].push(steepings);
        }
      } else {
        state.steepings[teaId] = steepings as Steeping[];
      }
    },

    removeSteeping(state, payload: { teaId: number, steepingId: number }) {
      const { teaId, steepingId } = payload;
      const newSteepings = state.steepings[teaId].filter((steeping) => steeping.id !== steepingId);
      state.steepings[teaId] = newSteepings;
    },
  },
  actions: {
    async addTea({ commit }, payload: { tea: Tea }) {
      const tea: Tea = await api.post('api/teas', { json: { ...payload.tea } }).json();
      commit('addTea', tea);
    },

    async removeTea({ commit }, payload: { teaId: number }) {
      await api.delete(`api/teas/${payload.teaId}`);
      commit('removeTea', payload);
    },

    async addSteeping({ commit }, payload: {steeping: Steeping}) {
      const steeping: Steeping[] = await api.post('api/steepings', { json: { ...payload.steeping } }).json();
      commit('addSteeping', steeping[0]);
    },

    async removeSteeping({ commit }, payload: { teaId: number, steepingId: number }) {
      await api.delete(`api/teas/${payload.teaId}/steepings/${payload.steepingId}`);
      commit('removeSteeping', payload);
    },
  },
  modules: {
  },
});
