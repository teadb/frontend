import ky from 'ky';

const api = ky.create({
  prefixUrl: process.env.VUE_APP_API_URL,
  headers: {
    Origin: process.env.VUE_APP_URL,
  },
  hooks: {
    beforeRequest: [
      (request) => {
        request.headers.set('Origin', process.env.VUE_APP_URL);
      },
    ],
  },
});

export default api;
