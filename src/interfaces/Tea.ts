/* TODO: should I use TS type here? */
interface Tea {
    id: number,
    name: string,
    country: string,
    description: string,
    // TODO: this is a pain, can TS like shut up or perform automatic conversion?
    // eslint-disable-next-line camelcase
    created_at: Date,
    // eslint-disable-next-line camelcase
    updated_at: Date
}

export default Tea;
