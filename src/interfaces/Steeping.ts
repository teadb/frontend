/* eslint-disable camelcase */
/* TODO: should I use TS type here? */
interface Steeping {
    id: number,
    rating: number,
    temperature: number,
    steeping_time: number,
    description: string,
    amount: number,
    tea_id: number,
    created_at: Date,
    updated_at: Date
}

export default Steeping;
