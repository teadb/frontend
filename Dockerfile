# build stage
FROM node:16.16.0-alpine3.16 as build-stage
WORKDIR /app
COPY . .
RUN corepack yarn install && \
    corepack yarn build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
