module.exports = {
  pwa: {
    workboxOptions: {
      runtimeCaching: [
        {
          urlPattern: new RegExp(`^${process.env.VUE_APP_API_URL}`),
          handler: 'NetworkFirst',
          options: {
            networkTimeoutSeconds: 5,
            cacheName: 'api-cache',
            cacheableResponse: {
              statuses: [0, 200],
            },
          },
        },
      ],
    },
  },
};
